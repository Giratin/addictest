package tn.esprit.spring.control;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import blog.article1.MajorModel;
import blog.article1.UniversityModel;
import tn.esprit.spring.entity.Major;
import tn.esprit.spring.entity.Question;
import tn.esprit.spring.entity.University;
import tn.esprit.spring.repository.MajorRepository;
import tn.esprit.spring.repository.UniversityRepository;
import tn.esprit.spring.service.MajorService;

@Controller
@RestController
@RequestMapping(value = "/major")
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
public class MajorController {

	
	@Autowired
	MajorRepository majRep;
	@Autowired
	UniversityRepository univRep;
	
	static List<Question> questions= new ArrayList();
	static List<Long> ok= new ArrayList();
	
	@PostMapping(value ="/createmajor")
	public Major CreateMajor(@Valid @RequestBody Major maj)
	{
		
		return majRep.save(maj) ;
	}
	
	@PostMapping(value ="/charge")
public void chargeBase() {
		
	    
		String baseUrl = "https://www.niche.com/colleges/american-university/rankings/" ;
		WebClient client = new WebClient();
		client.getOptions().setCssEnabled(false);
		client.getOptions().setJavaScriptEnabled(false);
		try {
			String searchUrl = baseUrl ;
			HtmlPage page = client.getPage(searchUrl);
			
			List<HtmlElement> items = (List<HtmlElement>) page.getByXPath("//li[@class='rankings-expansion__badge']") ;
			if(items.isEmpty()){
				System.out.println("No items found !");
			}else{
	
				for(HtmlElement htmlItem : items){
					HtmlElement itemAnchor = ((HtmlElement) htmlItem.getFirstByXPath(".//div[@class='rankings-card__link__title']"));
					HtmlAnchor itemUrl = ((HtmlAnchor) htmlItem.getFirstByXPath(".//a[@class='rankings-card__link']"));
					
					getUniversityByMajor(itemUrl.getHrefAttribute());
				
					ObjectMapper mapper = new ObjectMapper();
					
				}
			}
		} catch(Exception e){
			e.printStackTrace();
		}

	}
	 public void getUniversityByMajor(String url) {
		 
	    
		String baseUrl = url ;
		WebClient client = new WebClient();
		client.getOptions().setCssEnabled(false);
		client.getOptions().setJavaScriptEnabled(false);
		try {
			String searchUrl = baseUrl ;
			HtmlPage page = client.getPage(searchUrl);
			
			List<HtmlElement> items = (List<HtmlElement>) page.getByXPath("//div[@class='card']") ;
			if(items.isEmpty()){
				System.out.println("No items found !");
			}else{
				HtmlElement itemMajor = ((HtmlElement) items.get(0).getFirstByXPath(".//div[@class='search-result-badge']"));
				
				University university= new University();
				Major major = new Major();
				
				if(majorNale(itemMajor.asText()).contains("Hardest")||majorNale(itemMajor.asText()).contains("Best")||majorNale(itemMajor.asText()).contains("Top")||majorNale(itemMajor.asText()).contains("Most"))
				{
					System.out.println("CC");
				}else {
				major.setUniversities(new ArrayList<>());
		
				major.setTitle(majorNale(itemMajor.asText()));
				majRep.save(major);
				
				for(HtmlElement htmlItem : items){
					
					HtmlElement itemTitle  = ((HtmlElement) htmlItem.getFirstByXPath(".//h2[@class='search-result__title']"));
					HtmlAnchor itemUrl = ((HtmlAnchor) htmlItem.getFirstByXPath(".//a[@class='search-result__link']"));
				
					university = new University();
					university.setTitle(itemTitle.asText());
					if(univRep.findAll().contains(university))
					{
						university.getMajors().add(major);
						univRep.save(university);
					}else 
					{	//save university
						university.getMajors().add(major);
						univRep.save(university);						
					}
				}
				}
			}
		} catch(Exception e){
			e.printStackTrace();
		}

	}
public static String majorNale(String ch)
{
	
    int i =ch.indexOf("for", 0);
    String ne = ch.substring(i+4,ch.length());
    int i2 =ne.indexOf(" in",0) ;
    String toremove = ne.substring(i2, ne.length());
    
    return ne.replace(toremove , "");
}

@PostMapping(value ="/chargeUniv")
	
public void chargeUniversite() {
	

	
		WebClient client = new WebClient();
		client.getOptions().setCssEnabled(false);
		client.getOptions().setJavaScriptEnabled(false);
		String title="";
		try {
			for(University u : univRep.findAll()) {
				title=u.getTitle().replace(" ", "-");
				title=title.replace("&", "-and-");
				title=title.replace("'", "");
				title=title.replace(".", "");
			try {
				
			
			String searchUrl = "https://www.niche.com/colleges/"+title ;
			HtmlPage page = client.getPage(searchUrl);
			
			List<HtmlElement> items = (List<HtmlElement>) page.getByXPath("//section[@class='block--two-two']") ;
			if(items.isEmpty()){
				System.out.println("No items found !");
			}else{
				
					HtmlElement itemTitle  = ((HtmlElement) items.get(0).getFirstByXPath(".//div[@class='profile__bucket--3']"))
							.getFirstByXPath(".//div[@class='scalar__value']");
					if(itemTitle.equals(null))
						itemTitle  = ((HtmlElement) items.get(0).getFirstByXPath(".//div[@class='profile__bucket--4']"))
						.getFirstByXPath(".//div[@class='scalar__value']");
					u.setScore(itemTitle.asText());

					HtmlElement itemDeadline  = ((HtmlElement) items.get(0).getFirstByXPath(".//div[@class='profile__bucket--2']"))
							.getFirstByXPath(".//div[@class='scalar__value']");
					u.setDeadline(itemDeadline.asText());
					univRep.save(u);
					
				
					
				
			}}
			 catch (java.lang.NullPointerException e) {
				// TODO: handle exception
			}}
		} catch(Exception e){
			e.printStackTrace();
		}
	}
@PostMapping(value ="/chargeDes")

public void chargeDes() {
	

	
	WebClient client = new WebClient();
	client.getOptions().setCssEnabled(false);
	client.getOptions().setJavaScriptEnabled(false);
	String title="";
	try {
		for(University u : univRep.findAll()) {
			title=u.getTitle().replace(" ", "-");
			title=title.replace("&", "-and-");
			title=title.replace("'", "");
			title=title.replace(".", "");
		try {
			
		
		String searchUrl = "https://www.niche.com/colleges/"+title ;
		HtmlPage page = client.getPage(searchUrl);
		
		List<HtmlElement> items = (List<HtmlElement>) page.getByXPath("//section[@class='block--one']") ;
		if(items.isEmpty()){
			System.out.println("No items found !");
		}else{
			
				HtmlElement itemTitle  = ((HtmlElement) items.get(0).getFirstByXPath(".//div[@class='profile__bucket--1']"))
					.getFirstByXPath(".//span[@class='bare-value']");
				
				u.setDescription(itemTitle.asText());
				univRep.save(u);
				
			
				
			
		}}
		 catch (java.lang.NullPointerException e) {
			// TODO: handle exception
		}}
	} catch(Exception e){
		e.printStackTrace();
	}
}
@PutMapping(value ="/delete")
public void clean_universities()
{
	List<University> list =univRep.findAll();
	List<University> toRemove =new ArrayList<>();
	for(University u : list)
	{
		try {u.getDeadline().equals(null);}catch(NullPointerException n) {univRep.delete(u);}
		try {u.getScore().equals(null);}catch(NullPointerException n) {univRep.delete(u);}
		
	}
	
	//univRep.deleteAll(toRemove);
}

@GetMapping(value ="/show")
public void shoz()
{
	List<Major> list =(List<Major>) majRep.findAll();
	
	for(Major u : list)
	{
		System.out.println(u);
		
	}
	
	
}

public void questionReponse()
{
	List<Major> G1 = new ArrayList<>();
	G1.add(majRep.getOne((long)5));
	G1.add(majRep.getOne((long)11));
	G1.add(majRep.getOne((long)6));
	
	List<Major> G2 = new ArrayList<>();
	G2.add(majRep.getOne((long)5));
	G2.add(majRep.getOne((long)1));
	G2.add(majRep.getOne((long)3));
	
	List<Major> G3 = new ArrayList<>();
	G3.add(majRep.getOne((long)10));
	G3.add(majRep.getOne((long)1));
	G3.add(majRep.getOne((long)9));
	
	List<Major> G4 = new ArrayList<>();
	G4.add(majRep.getOne((long)14));
	G4.add(majRep.getOne((long)8));
	G4.add(majRep.getOne((long)7));
	G4.add(majRep.getOne((long)12));
	
	List<Major> G5 = new ArrayList<>();
	List<Major> G6 = new ArrayList<>();
	List<Major> G7 = new ArrayList<>();
	List<Major> G8 = new ArrayList<>();
	
	G5.add(majRep.getOne((long)16));
	G5.add(majRep.getOne((long)4));
	G6.add(majRep.getOne((long)15));
	G7.add(majRep.getOne((long)2));
	G8.add(majRep.getOne((long)13));
	
	
}

@PostMapping("/start")
public ResponseEntity<Question> startQuestion()
{
Question q = new Question((long)1, "Do you enjoy reading about history ?", false, (long)6);
questions.add(q);
Question q1 = new Question((long)2, "Are you interested in law , debate ,gouverment and politics?", true, (long)5);
questions.add(q1);
Question q3 = new Question((long)3, "Do you enjoy helping people?", false, (long)11);
questions.add(q3);
List<Long> grp = new ArrayList<>();
grp.add((long) 5);
grp.add((long) 11);
grp.add((long) 6);
q.setMajor_group(grp);
Question q4 = new Question((long)4, "Do you have strong verbal and written communication skills ?", false, (long)3);
questions.add(q4);
Question q5= new Question((long)5, "Do you prefer face to face conversation?", true, (long)1);
questions.add(q5);
Question q6 = new Question((long)6, "Are you interested in cross-culural?", false, (long)5);
questions.add(q6);
List<Long> grp1 = new ArrayList<>();
grp1.add((long) 5);
grp1.add((long) 1);
grp1.add((long) 3);
q4.setMajor_group(grp1);
//
Question q7 = new Question((long)7, "Do you think you're excellent in english ?", false, (long)10);
questions.add(q7);
Question q8 = new Question((long)8, "Are you interested in intellectual ideas ,philosophical or religious ?", true, (long)1);
questions.add(q8);
Question q9 = new Question((long)9, "Do you enjoy learning and teaching languages?", false, (long)9);
questions.add(q9);
List<Long> grp2 = new ArrayList<>();
grp2.add((long)9);
grp2.add((long) 10);
grp2.add((long) 1);
q7.setMajor_group(grp2);
//
Question q10 = new Question((long)10, "Are you good with numbers and a detail-oriented ?", false, (long)14);
questions.add(q10);
Question q11 = new Question((long)11, "Do you like math and figuring out how things work ?", true, (long)12);
questions.add(q11);
Question q12 = new Question((long)12, "Are you intersted about international economics?", false, (long)8);
questions.add(q12);
Question q13 = new Question((long)13, "Do you like business management?", false, (long)7);
questions.add(q13);
List<Long> grp3 = new ArrayList<>();
grp3.add((long) 14);
grp3.add((long) 12);
grp3.add((long) 8);
grp3.add((long) 7);
q10.setMajor_group(grp3);
//
Question q14 = new Question((long)14, "Do you enjoy learnig about how the humain body and natural world work ?", true, (long)13);
questions.add(q14);
Question q15 = new Question((long)15, "Are you interested in science and health care?", false, (long)13);
questions.add(q15);
List<Long> grp4 = new ArrayList<>();
grp4.add((long) 13);
grp4.add((long) 13);

q14.setMajor_group(grp4);
//
Question q16 = new Question((long)16, "Are you interseted in conflict resolution criminal justice or mediation ?", false, (long)2);
questions.add(q16);
List<Long> grp5 = new ArrayList<>();
grp5.add((long) 2);

q16.setMajor_group(grp5);
//
Question q17 = new Question((long)17, "Are you intereseted in technology and learning how computers work  ?", true, (long)16);
questions.add(q11);
List<Long> grp6 = new ArrayList<>();
grp6.add((long) 16);

q17.setMajor_group(grp6);
//
Question q18 = new Question((long)18, "Are you interested in graphic and/or web design ?", false, (long)4);
questions.add(q18);
Question q19 = new Question((long)19, "Do you like to sing or play musical instruments ?", true, (long)4);
questions.add(q19);
Question q20 = new Question((long)20, "Do you strong interest and ability in visual art  ?", false, (long)4);
questions.add(q20);
List<Long> grp7 = new ArrayList<>();
grp7.add((long)4);
grp7.add((long) 4);
grp7.add((long) 4);
q18.setMajor_group(grp7);



	Random rand = new Random();
	q=questions.get(rand.nextInt(questions.size()));
	
	questions.remove(q);
	return new ResponseEntity<Question>(q,HttpStatus.OK);
	}


@PostMapping("/push")
public ResponseEntity<Question> generateQuestion(@RequestBody Question q1)
{
	if(!q1.getAnswer1())
	{
		for(Long i:q1.getMajor_group())
			questions.remove(q1);
	}else ok.add(q1.getAffected_major());
	
		Question q = new Question();
		if(ok.size()==5)
		{
			q.setId((long) 0);
		}else {
			Random rand = new Random();
			q=questions.get(rand.nextInt(questions.size()));
			questions.remove(q);
		}
	
	return new ResponseEntity<Question>(q,HttpStatus.OK);
	}
@PostMapping("/viewAnswer")
public ResponseEntity<List<MajorModel>> answer()
{
	List<MajorModel> list = new ArrayList();
	for(Long i:ok) {
		Major m = majRep.getOne(i);
		MajorModel mm= new MajorModel(m.getId(), m.getTitle());
		list.add(mm);
	}
	return new ResponseEntity<List<MajorModel>>(list,HttpStatus.OK);
	}
@PostMapping("/univ")
public ResponseEntity<List<UniversityModel>> universities(@RequestBody MajorModel mojar)
{
	List<UniversityModel> list = new ArrayList();
	
		Major m = majRep.getOne(mojar.getId());
		System.out.println(m);
		for(int i=0;i<6;i++)
		{
			UniversityModel um = new UniversityModel(m.getUniversities().get(i).getId(), m.getUniversities().get(i).getLocation(), 
					m.getUniversities().get(i).getTitle(), m.getUniversities().get(i).getDescription(), 
					m.getUniversities().get(i).getScore(),  m.getUniversities().get(i).getDeadline());
			list.add(um);
		}
		
	
	return new ResponseEntity<List<UniversityModel>>(list,HttpStatus.OK);
	}
}


	
	

