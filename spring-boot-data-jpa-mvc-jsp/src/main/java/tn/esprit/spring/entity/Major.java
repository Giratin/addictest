package tn.esprit.spring.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.ManyToAny;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;



@Entity
public class Major implements Serializable {
	
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Long id; 
	private String title;
	
	@JsonBackReference
	
	@Fetch(value = FetchMode.SUBSELECT)
	@ManyToMany(mappedBy="majors",fetch = FetchType.EAGER)
	private List<University> universities;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	public List<University> getUniversities() {
		return universities;
	}
	public void setUniversities(List<University> universities) {
		this.universities = universities;
	}
	@Override
	public String toString() {
		return "Major [id=" + id + ", title=" + title + ", universities=" + universities + "]";
	}
	

}
