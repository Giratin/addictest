package blog.article1;

public class UniversityModel {
	private Long id;
	private String location;
	private String title;
	private String description;
	private String score;
	
	private String deadline;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public String getDeadline() {
		return deadline;
	}

	public void setDeadline(String deadline) {
		this.deadline = deadline;
	}

	public UniversityModel(Long id, String location, String title, String description, String score, String deadline) {
		super();
		this.id = id;
		this.location = location;
		this.title = title;
		this.description = description;
		this.score = score;
		this.deadline = deadline;
	} 
	
}
