package blog.article1;

public class MajorModel {
	private Long id; 
	private String title;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public MajorModel(Long id, String title) {
		super();
		this.id = id;
		this.title = title;
	}
	
	

}
